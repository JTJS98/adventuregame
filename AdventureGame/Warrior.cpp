#include "Warrior.h"
#include <iostream>



Warrior::Warrior()
{
}

Warrior::Warrior(std::string name) :Player(name)
{
	setDescription("I am a warrior!");
	setHealth(10);
	setAttack(12);
	setLevel(1);
}


Warrior::~Warrior()
{
	std::cout << "Red Warrior needs food badly..." << std::endl;
}
