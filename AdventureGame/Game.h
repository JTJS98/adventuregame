#pragma once
#include "Thief.h"
#include "Warrior.h"

class Game
{
public:
	Game();
	~Game();

	void run();
	void update();

	Player player;	

private:
	void playerSelect();


	bool gameOver = false;
};



void Game::run()
{
	
	while (!gameOver)
	{
		update();
	}
	
}

void Game::update()
{
	std::cout << "update..." << std::endl;
	gameOver = true;
}