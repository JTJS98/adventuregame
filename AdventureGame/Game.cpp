#include "Game.h"
#include <string>
#include <iostream>
#include <climits>


Game::Game()
{
}

Game::~Game()
{
}

void Game::run()
{
	playerSelect();

	while (!gameOver)
	{
		update();
	}

}

void Game::update()
{
	std::cout << "update..." << std::endl;
	gameOver = true;
}

void Game::playerSelect()
{
	//this is where we will select our player type
	//get the name
	std::string name;
	std::cout << "What is your name, brave hero?\n>>";
	std::cin >> name;
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');


	bool done = false;

	while (!done)
	{
		
		//get the player type
		std::string playerType;
		std::cout << "What player type do you wish to be? \n";
		std::cout << "1. Warrior\n2. Thief\n\n>>";
		std::cin >> playerType;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		if (playerType == "warrior")
		{
			//create a warrior
			player = Warrior(name);
			std::cout << player.getDescription() << std::endl;
			done = true;
		}
		else if (playerType == "thief")
		{
			//create a thief
			player = Thief(name);
			std::cout << player.getDescription() << std::endl;
			done = true;
		}
		else
		{
			//no idea what the user did. Try again.
		}

	}


}